import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person.js';
class App extends Component {
  // A state is a type of property managed from inside a component(available to anything that Extends Component
  // It's not available as function component). State can be changed, if it changes it will lead react to re-redner
  //the DOM.
  state = {
    //persons aray
    persons: [
      //js objects
      { name: 'James', age: 28 },
      { name: 'Debug', age: 32 },
      { name: 'Debug',  age: 31 }
    ]
  }

  switchNameHandler = () => {
    console.log('Was Clicked.')
  }
// This is the same as the one below, its simpler and jsx compiles to the bottom
  render() {
    return (
      <div className="App">
        <p>"This is where the core is rendered. Each module necessary is"</p>
        <p>added here afterword</p>
        <h1>Hey there, Jesse.</h1>
        <button onClick={this.switchNameHandler()}>Switch Name</button>
        <Person name={this.state.persons[0].name} age={this.state.persons[0].age}></Person>
        <Person name={this.state.persons[1].name} age={this.state.persons[1].age}>My Hobbies: eating forest twigs.</Person>
        <Person name={this.state.persons[2].name} age={this.state.persons[2].age}></Person>
      </div>
    );
  //return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Hey Jesse, miss ya.'))
  }
}

export default App;
