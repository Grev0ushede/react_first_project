import React from 'react';
  //props are set and passed from outside
const person = (props) => {
  return (
    <div>
      <p>I am a person component named {props.name}. The amount of time I wasted on this planet is {props.age} years or {Math.floor(props.age * 365)} days. Totaling {props.age * 8760} hours alive so far.</p>
      <p>{props.children}</p>
    </div>
  )
};

export default person;
